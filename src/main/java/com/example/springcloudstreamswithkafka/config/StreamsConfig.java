package com.example.springcloudstreamswithkafka.config;

import com.example.springcloudstreamswithkafka.streams.GreetingsStreams;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(GreetingsStreams.class)
public class StreamsConfig {

}
