package com.example.springcloudstreamswithkafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudStreamsWithKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudStreamsWithKafkaApplication.class, args);
    }

}

